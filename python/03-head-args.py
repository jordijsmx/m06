#!/usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024
# ------------------------------

# head [ -n nlin ] [ -f file ]
# default = 10, file o stdin
# -----------------------------
import sys, argparse

parser = argparse.ArgumentParser(\
        description = "Mostrar les N primeres línees",\
        prog = "03-head-args.py",\
        epilog = "That's all folks!")

parser.add_argument("-n","--nlin",type = int,\
        dest = "nlin",\
        metavar = "numLines",\
        default = 10,\
        help = "número de línees")

parser.add_argument("-f","--fit", type = str,\
        dest = "fitxer",
        help = "fitxer a processar",
        metavar = "fitxer")

args = parser.parse_args()
print(args)
# ----------------------------
MAX=args.nlin
counter=0

fileIn = open(args.fitxer,"r")

for line in fileIn:
    counter += 1
    print(line,end="") 

    if counter == MAX: break

fileIn.close()
exit(0)

