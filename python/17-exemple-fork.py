#! /usr/bin/python3
#-*- coding: utf-8-*-
# @jordijsmx ASIX M06 Curs 2023-2024
# -----------------------------------

import sys, os

print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork() # Duplica el procés igual amb un PID nou (procés fill)

if pid !=0:     # os.wait() espera a que finalitzi el procés fill
 # os.wait()        # aquesta part només s'executa si ets el pare
  print("Programa pare: ",os.getpid(),pid)
else:
  print("Programa fill: ",os.getpid(),pid)  # només s'executa l'else si ets el fill
  while True:
    pass

print("Hasta luego Lucas!")
sys.exit(0)
    
