#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# daytime-server.py
# --------------------------------------
import sys,os,socket,argparse,signal
from subprocess import Popen,PIPE

parser=argparse.ArgumentParser(description="Server")
parser.add_argument("-p","--port",type=int,default=50001)

args=parser.parse_args()
HOST=''
PORT=args.port

llistaPeers = []

# ------------------------------------

def myusr1(signum,frame):
    print(llistaPeers)
    sys.exit(0)

def myusr2(signum,frame):
    print(len(llistaPeers))
    sys.exit(0)

def myterm(signum,frame):
    print(listaPeers, len(llistaPeers))
    sys.exit(0)

signal.signal(signal.SIGUSR1,myusr1)
signal.signal(signal.SIGUSR2,myusr2)
signal.signal(signal.SIGTERM,myterm)

# -------------------------------------

pid=os.fork()
if pid != 0:
    print("Engegat server ss",pid)
    sys.exit(0)

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
s.bind((HOST,PORT))
s.listen(1)

#--------------------------

while True:
    conn,addr=s.accept()
    print("Connected by",addr)
    llistaPeers.append(addr)
    command="ss -ltn"
    pipeData=Popen(command,shell=True,stdout=PIPE)

    for line in pipeData.stdout:
        conn.send(line)
    conn.close()
