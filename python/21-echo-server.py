#! /usr/bin/python3
#-*- coding: utf-8-*-
# @jordijsmx ASIX M06 Curs 2023-2024
# -----------------------------------

import sys,socket

# no posar res equival a localhost
HOST=''
PORT=50001

# Crea un objecte de tipus socket
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)  # STREAM: protocol TCP orientat a connexió
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

# Escoltara pel HOST I PORT definits
s.bind((HOST,PORT))
# que es posi a escoltar
s.listen(1)
# es queda clavant esperant una connexió d'entrada
conn, addr=s.accept() # s.accept() ens torna una tupla però es desa en les 2 variables

# conn és una variable que representa una connexió ja establerta
# s és una variable que representa un socket, un socket escolta per un port
print("Conn: ", type(conn),conn)
print("Connected by: " ,addr)

while True:
    data = conn.recv(1024)

# no vol dir si no hi han dades, vol dir si HAN TANCAT LA CONNEXIÓ

    if not data: break 

    conn.send(data)
    print(data)

conn.close()
sys.exit(0)


