#!/usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024
# ------------------------------
# head [ -n 5|10|15 ] [ -f file ] la n pot ser 5 10 o 15
# default = 10, file o stdin
# Com a minim un argument serà obligatori !! 
# -----------------------------
import sys, argparse

parser = argparse.ArgumentParser(\
        description = "Mostrar les N primeres línees",\
        prog = "03-head-args.py",\
        epilog = "That's all folks!")

parser.add_argument("-n","--nlin",type = int,\
        dest = "nlin",\
        metavar = "numLines",\
        choices = [5,10,15],\
        default = 10,\
        help = "número de línees")

parser.add_argument("fitxer", type = str,\
        help = "fitxer a processar",\
        metavar = "file")

args = parser.parse_args()
print(args)
# ----------------------------
MAX=args.nlin
counter=0

fileIn = open(args.fitxer,"r")

for line in fileIn:
    counter += 1
    print(line,end="") 

    if counter == MAX: break

fileIn.close()
exit(0)

