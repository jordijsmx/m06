#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# @jordijsmx ASIX M06 Curs 2023-2024
# programa.py UNA SOLA RUTA POSITIONAL ARGUMENT
# ----------------------------------
import sys
import argparse

from subprocess import Popen,PIPE # Això ho podem fer sempre i quant no hi hagi conflicte de nom de paquets.
# ---------------------------------

parser = argparse.ArgumentParser(\
        description = "Exemple popen")

parser.add_argument("ruta", type = str,\
        help = "driectori a llistar")

args = parser.parse_args()

# Executa ordre who del sistema
command = ["ls", args.ruta]

# Defineix una variable qualsevol
# Popen és un constructor, que construeix un pipe
# stdout=PIPE, la sortida del who va a parar al pipe
pipeData = Popen(command, stdout=PIPE)

# La llegeix i la mostra per pantalla
# who ---> PIPE ---> python
# python llegeix de la sortida del pipe
for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")

exit (0)
