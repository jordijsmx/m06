#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# @jordijsmx ASIX M06 Curs 2023-2024

# passar-li com a arguments -c num_cli -c num_cli... 
# ------------------------------------

import sys
import argparse
from subprocess import Popen,PIPE

# -----------------------------------
parser = argparse.ArgumentParser(description=\
        """Consulta a la base de dades entrada per argument al programa.""")

#parser.add_argument("sqlStatement",type=str,\
#        help="consulta a la base de dades training")

parser.add_argument("-c","--con", type = str,\
        action = "append",\
        help = "num client",\
        dest = "fileList",\
        metavar = "file")

args=parser.parse_args()

#-----------------------------------

command = ["PGPASSWORD=passwd psql -qtA -F ',' -h localhost -U postgres training"]

# Definim el tubo i subprocés
pipeData = Popen(command, shell=True, bufsize=0,\
        universal_newlines=True,\
        stdin=PIPE, stdout=PIPE, stderr=PIPE)

for num_clie in args.fileList:
    sqlStatement = "select * from clientes where num_clie=%s;" % (num_clie)
    pipeData.stdin.write(sqlStatement+"\n")
    print(pipeData.stdout.readline(), end="")

pipeData.stdin.write("\q\n")

exit (0)

