#!/usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024
# ------------------------------

# Programa que realitza un head d'un fitxer passat per argument
# Processa argument, si n'hi ha, si no, processa stdin

import sys

MAX=5
counter=0

fileIn = sys.stdin

if len(sys.argv) == 2:
    fileIn=open(sys.argv[1],"r")

for line in fileIn:
    counter += 1
    print(line,end="") 

    if counter == MAX: break

fileIn.close()
exit(0)

