#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# @jordijsmx ASIX M06 Curs 2023-2024

# psql ha d'engegar i popen 
# ------------------------------------

import sys
import argparse
from subprocess import Popen,PIPE

# -----------------------------------
parser = argparse.ArgumentParser(description=\
        """Consulta a la base de dades entrada per argument al programa.""")

parser.add_argument("sqlStatement",type=str,\
        help="consulta a la base de dades training")

args=parser.parse_args()
#-----------------------------------

command = ["PGPASSWORD=passwd psql -qtA -F ',' -h localhost -U postgres training"]

# Definim el tubo i subprocés
pipeData = Popen(command, shell=True, bufsize=0,\
        universal_newlines=True,\
        stdin=PIPE, stdout=PIPE, stderr=PIPE)

# Entrada del tubo del cantó del procés python
pipeData.stdin.write(args.sqlStatement+"\n\q\n")

for line in pipeData.stdout: # el bucle  fa un readline linea a linea
    print(line,end="")

exit (0)

