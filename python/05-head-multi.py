#!/usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024
# ------------------------------

# head [ -n 5|10|15 ] [ -f file ] ...
# default = 10, file o stdin
# -----------------------------
import sys, argparse

parser = argparse.ArgumentParser(\
        description = "Mostrar les N primeres línees",\
        prog = "03-head-args.py",\
        epilog = "That's all folks!")

parser.add_argument("-n","--nlin",type = int,\
        dest = "nlin",\
        metavar = "numLines",\
        choices = [5,10,15],\
        default = 10,\
        help = "número de línees")

parser.add_argument("-f","--fit", type = str,\
        action = "append",\
        help = "fitxer a processar",\
        dest = "fileList",\
        metavar = "file")

args = parser.parse_args()
print(args)

# ----------------------------
MAX=args.nlin

def headFile(fitxer):
    fileIn = open(fitxer,"r")
    counter = 0
    for line in fileIn:
        counter += 1
        print(line,end="") 

        if counter == MAX: break

    fileIn.close()

for fileName in args.fileList:
    headFile(fileName)

exit(0)

