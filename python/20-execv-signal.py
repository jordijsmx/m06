#! /usr/bin/python3
#-*- coding: utf-8-*-
# @jordijsmx ASIX M06 Curs 2023-2024
# -----------------------------------

import sys, os

print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork() 

if pid !=0:     
 # os.wait()    
  print("Programa pare: ",os.getpid(),pid)
  sys.exit(0)

# programa fill
#os.execv("/usr/bin/ls",["ls", "-la","/"]) # v: s'ha de posar en llista o array 
#os.execl("/usr/bin/ls","ls","-la","/") # l: es posen els arguments un darrere l'altre
#os.execlp("ls","ls","-la","/") # resol el PATH amb la p
#os.execvp("uname",["uname","-a"])  # PATH i llista
#os.execv("/bin/bash",["bash","show.sh"])

# executar aquest programa show.sh i li passem els valors de les variables, nom=joan, edat=24
#os.execle("/bin/bash","bash","show.sh",{"nom":"joan","edat":"25"})

# enlloc de ser el procés actual es transforma en un altre proces, a dins dels []
# tot el que hi ha abans de execv, es perd, ja que es substitueix pel que hi hagi
# al execv
os.execv("/usr/bin/python3",["python3","16-signal.py"])


print("Hasta luego Lucas!")
sys.exit(0)
    
