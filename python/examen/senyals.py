#! /usr/bin/python3 
#-*- coding: utf-8 -*-

import sys,os,signal

def myusr1(signum,frame):
    print("S'ha rebut el senyal", signum)


print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    signal.signal(signal.SIGUSR1, myusr1)

    os.wait()
    print("Programa pare: ", os.getpid(),pid)
    print("Començant procés fill")
    sys.exit(0)

print("Programa fill: ", os.getpid(), pid)

os.kill(os.getppid(), signal.SIGUSR1)

sys.exit(0)
