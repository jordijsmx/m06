import os

def create_pipe(pipe_name):
    try:
        os.mkfifo(pipe_name)
        print(f"Named pipe '{pipe_name}' creat.")
    except OSError as oe:
        print(f"Error en crear el named pipe '{pipe_name}': {oe}")

if __name__ == "__main__":
    pipe_name = "mypipe"
    create_pipe(pipe_name)

