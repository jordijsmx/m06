def read_from_pipe(pipe_name):
    with open(pipe_name, 'r') as pipe_in:
        message = pipe_in.read()
        print(f"Llegint del named pipe '{pipe_name}': {message}")

if __name__ == "__main__":
    pipe_name = "mypipe"
    read_from_pipe(pipe_name)

