def write_to_pipe(pipe_name, message):
    with open(pipe_name, 'w') as pipe_out:
        pipe_out.write(message)
        print(f"Escrivint al named pipe '{pipe_name}': {message}")

if __name__ == "__main__":
    pipe_name = "mypipe"
    message = "Hola, aquest és un missatge de prova!"
    write_to_pipe(pipe_name, message)

