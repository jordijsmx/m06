#! /usr/bin/python3
#-*- coding: utf-8-*-
# @jordijsmx ASIX M06 Curs 2023-2024
# -----------------------------------

import sys, os

print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork() # Duplica el procés igual amb un PID nou (procés fill)

def myusr1(signum,frame):
    print("Signal handler called with signal:", signum)
    print("Hola!")

def myusr2(signum,frame):
    print("Signal handler called with signal:", signum)
    print("Adéu!")
    sys.exit(0)


if pid !=0:     # os.wait() espera a que finalitzi el procés fill
 # os.wait()        # aquesta part només s'executa si ets el pare
  print("Programa pare: ",os.getpid(),pid)
  print("Llançant el procés fill servidor.")
  sys.exit(0)



print("Programa fill: ",os.getpid(),pid) 
signal.signal(signal.SIGUSR1,myusr1)  #10
signal.signal(signal.SIGUSR2,myusr2)  #12 

while True:
    pass

# no s'executarà mai
print("Hasta luego Lucas!")
sys.exit(0)
    
