#! /usr/bin/python3
#-*- coding: utf-8-*-
# @jordijsmx ASIX M06 Curs 2023-2024
#
# Assignar un handler al senyal
# ----------------------------------

import sys,os,signal

# DEFINIM OBJECTES DELS HANDLERS
def myhandler(signum,frame):
    print("Signal handler with signal: ", signum)
    print("Hasta luego lucas!")
    sys.exit(0)
    
def nodeath(signum, frame):
    print("Signal handler with signal: ", signum)
    print("No em dona la gana de plegar.")
    
# HANDLERS
signal.signal(signal.SIGUSR1,myhandler) #10
signal.signal(signal.SIGUSR2,nodeath) #12
signal.signal(signal.SIGALRM,myhandler) #14

signal.signal(signal.SIGINT,signal.SIG_IGN) #2   IGNORA
signal.signal(signal.SIGTERM,signal.SIG_IGN) #15  IGNORA

signal.alarm(30)  # timer

print(os.getpid()) # mostra PID abans de començar el bucle

# bucle infinit

while True:
    pass               # pass = no fa res
sys.exit(0)         # mi preferido
