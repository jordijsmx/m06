#!/usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024
# ------------------------------

# head [ -n 5|10|15 ] -v file... si no hi ha V es comporta igual, si hi ha, abans, de cada header es fa el print del nom del fitxer
# default = 10
# -----------------------------
import sys, argparse

parser = argparse.ArgumentParser(\
        description = "Mostrar les N primeres línees",\
        prog = "03-head-args.py",\
        epilog = "That's all folks!")

parser.add_argument("-n","--nlin",type = int,\
        dest = "nlin",\
        metavar = "numLines",\
        choices = [5,10,15],\
        default = 10,\
        help = "número de línees")

parser.add_argument("-v","--verbose", action = "store_true")
parser.add_argument("fileList", type = str,\
        help = "fitxer a processar",\
        metavar = "file",\
        nargs = "*")

args = parser.parse_args()
print(args)

# ----------------------------
MAX=args.nlin

def headFile(fitxer):
    fileIn = open(fitxer,"r")
    counter = 0
    for line in fileIn:
        counter += 1
        print(line,end="") 

        if counter == MAX: break

    fileIn.close()

if args.fileList:
    for fileName in args.fileList:
        if args.verbose: 
            print("\n",fileName, 33*"-")
        headFile(fileName)

exit(0)
