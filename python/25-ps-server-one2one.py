#! /usr/bin/python3
#-*- coding: utf-8-*-
# @jordijsmx ASIX M06 Curs 2023-2024
# -----------------------------------
import sys, os, socket, argparse, time
from subprocess import Popen, PIPE
from datetime import datetime

parser = argparse.ArgumentParser(description="""ps server""")
parser.add_argument("-p","--port", type=int, default=50001)
args = parser.parse_args()

HOST = ''
PORT = args.port

llistapeers=[]
# ---------------------------------

pid=os.fork()

if pid !=0:
  print("Server PS:", pid)
  sys.exit(0)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
s.bind((HOST,PORT))
s.listen(1)

# bucle
while True:    # per cada client
    conn, addr = s.accept()
    print("Connected by", addr)
    llistapeers.append(addr)
    command="ps ax"
    
    # genera el nom
    fitxerNom = "/tmp/%s-%s-%s.log"%(addr[0], addr[1], time.strftime("%Y%m%d-%H%M%s"))
    
    # obrir el fitxer
    fitxer = open(fitxerNom, "w")
    
    while True:      # mentre hi hagi dades
        data = conn.recv(1024)
        if not data: break
        fitxer.write(str(data))
    
    conn.close()
    fitxer.close()
