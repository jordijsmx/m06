#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# @jordijsmx ASIX M06 Curs 2023-2024
# ------------------------------------

import sys
import argparse
from subprocess import Popen,PIPE

parser = argparse.ArgumentParser(\
        description = "Exemple popen")

args = parser.parse_args()
command = ["psql -qtA -F ',' -h localhost -U postgres training -c \"select * from clientes; \""]
pipeData = Popen(command, shell=True,stdout=PIPE)

for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")

exit (0)

