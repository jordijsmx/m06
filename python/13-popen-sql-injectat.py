#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# @jordijsmx ASIX M06 Curs 2023-2024

# psql ha d'engegar i popen 
# ------------------------------------

import sys
import argparse
from subprocess import Popen,PIPE

command = ["PGPASSWORD=passwd psql -qtA -F ',' -h localhost -U postgres training"]

# Definim el tubo i subprocés
pipeData = Popen(command, shell=True, bufsize=0,\
        universal_newlines=True,\
        stdin=PIPE, stdout=PIPE, stderr=PIPE)

# Entrada del tubo del cantó del procés python
pipeData.stdin.write("select * from oficinas;\n\q\n")

for line in pipeData.stdout: # el bucle  fa un readline linea a linea
    print(line,end="")

exit (0)

