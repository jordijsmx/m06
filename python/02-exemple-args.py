#!/usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024
# ------------------------------

import argparse

# Constructor
parser = argparse.ArgumentParser(\
        description = "Programa d'exemple d'arguments",\
        prog = "02-arguments.py",\
        epilog = "hasta luego lucas!")

# afegeix un argument a una variable
parser.add_argument("-n","--nom",type = str,\
        help = "nom usuari")

parser.add_argument("-e","--edat", type = int,\
        dest = "userEdat",
        help = "edat a processar",
        metavar = "edat")

args = parser.parse_args() # crido a l'objecte parser per processar els arguments del constructor

print(args) # args és un diccionari
print(args.userEdat,args.nom) # necessitem el format diccionari de args
exit(0)
