#! /usr/bin/python3
#-*- coding: utf-8-*-
# @jordijsmx ASIX M06 Curs 2023-2024
# -----------------------------------
import sys, os, socket, argparse, time
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""ps server""")
parser.add_argument("-p","--port", type=int, default=50001)
args = parser.parse_args()

HOST = ''
PORT = args.port
MYEOF = bytes(chr(4), 'utf-8')

# -----------------------------------

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

pid=os.fork()
if pid !=0:
  print("Server PS:", pid)
  sys.exit(0)

while True:
  conn, addr = s.accept()
  print("Connected by", addr)

  while True:
    data = conn.recv(1024)
    if not data : break
    pipeData = Popen(data,stdout=PIPE,shell=True)
    for line in pipeData.stdout:
        conn.sendall(line)
    conn.send(MYEOF)
  conn.close()
