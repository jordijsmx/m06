# Servidor LDAP	en detach versió config

## Fitxers necessaris al directori context
```
Dockerfile
edt-org.ldif
jordi-org.ldif
slapd.conf
startup.sh
```
## Creació de domini propi

Crearem la nostre base de dades ldap en el fitxer **jordi-org.ldif**
```
dn: dc=jordi,dc=org
dc: jordi
description: organitzacio de en pere
objectClass: dcObject
objectClass: organization
o: jordi.org

dn: ou=empleats,dc=jordi,dc=org
ou: empleats
description: Container per a persones empleades
objectclass: organizationalunit

dn: cn=Pau Pou,ou=empleats,dc=jordi,dc=org
objectclass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homephone: 555-222-2220
mail: pau@jordi.org
description: Watch out for this guy

dn: cn=Anna Pou,ou=empleats,dc=jordi,dc=org
objectclass: inetOrgPerson
cn: Anna Pou
cn: Annita Pou
sn: Pou
homephone: 555-222-2221
mail: anna@jordi.org
description: Watch out for this girl
```
## Contingut slapd.conf
Modificarem l'arxiu de configuració per afegir la nova base de dades i, a més, una base de dades **cn=config** per administrar les bases de dades del LDAP
```
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
# debian packages: slapd ldap-utils

include		/etc/ldap/schema/corba.schema
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/duaconf.schema
include		/etc/ldap/schema/dyngroup.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/java.schema
include		/etc/ldap/schema/misc.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema
#include		/etc/ldap/schema/ppolicy.schema
include		/etc/ldap/schema/collective.schema

# Allow LDAPv2 client connections.  This is NOT the default.
allow bind_v2

pidfile		/var/run/slapd/slapd.pid
#argsfile	/var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la

# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw secret
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ---------------------------------------------------------------------
database mdb
suffix "dc=jordi,dc=org"
rootdn "cn=mymanager,dc=jordi,dc=org"
rootpw jordi
directory /var/lib/ldap-jordi
index objectClass eq,pres
access to * by * write
# ----------------------------------------------------------------------
database config
rootdn "cn=Sysadmin,cn=config"
rootpw syskey
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
# ----------------------------------------------------------------------
```
## Generar la imatge
```
docker build -t jordijsmx/ldap:config .
```
## Arrancar la imatge
```
docker run --rm -h ldap.edt.org -it jordijsmx/ldap:config /bin/bash
```
## Arrancar la imatge en detach
```
docker run --rm --name ldap -h ldap.edt.org -d jordijsmx/ldap:config
```
## Entrar al container engegat
```
docker exec -it ldap /bin/bash
```
