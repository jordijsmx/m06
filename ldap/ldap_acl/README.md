# ACL

# generar imatge
```
docker run --rm --name acl -h ldap.edt.org --net 2hisx -d jordijsmx/ldap:acl
```
# Arrancar imatge
```
docker run --rm --name acl -h ldap.edt.org --net 2hisx -d jordijsmx/ldap:acl
```
# Llistar la configuració de la base de dades 1
```
ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b "olcDatabase={1}mdb,cn=config"
```
# Llistar atributs de les ACL
```
ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b "olcDatabase={1}mdb,cn=config" olcAccess
```
# Modificar atributs de les ACL
```
ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f acl1.ldif
```
# Contingut acl1.ldif
```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcAccess
-
add: olcAccess
olcAccess: to * by * read
```
# Copiat del fitxer /etc/ldap/ldap.conf
```
docker cp acl:/etc/ldap/ldap.conf .
```
# Consulta com a anònim sense introduïr dades
```
ldapsearch -x -LLL dn mail
```
