# LDAP Server imatges

## [Dockerhub](https://hub.docker.com/)

## LDAP Servers:
- **jordijsmx/ldap:base** Servidor amb les dades de edt.org.  
- **jordijsmx/ldap:config** Servidor amb nova base de dades pròpia i base de dades cn=config a slapd.conf
- **jordijsmx/ldap:editat** Servidor amb les dades de edt.org amb els canvis:
  - user01-user10 amb password xifrat
  - 2 ou noves i 2 usuaris a dins d'aquestes ou
  - Password de manager xifrat
  - Base de dades cn=config a slapd.conf
