# LDAP server

## Exercici Ldap23:grups

Imatge ldap amb una nova Organizational Unit anomenada grups el qual li assignarem a noves entitats.

### Desplegament
```
docker build -t jordijsmx/ldap:grups .

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -p 636:636 -d jordijsmx/ldap:grups
```
#### Usuaris amb RDN en format UID
Modifiquem els RDN dels usuaris per a que s'identifiquin pel seu UID en el DN.
```
dn: uid=user01,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user01
cn: usuari 1
sn: usuari 1
homephone: 555-222-0001
mail: user01@edt.org
description: Watch out for this user
ou: 1asix
uid: user01
uidNumber: 7001
gidNumber: 610
homeDirectory: /tmp/home/1asix/user01
userPassword: {SHA}BJf+TWdP43GUpvywiRPllu9qMH8=
```
#### Entitat OU grups
```
dn: ou=grups,dc=edt,dc=org
ou: grups
description: Container per als grups
objectclass: organizationalunit
```
#### Grups amb entitats de tipus posixAccount
- professors / 601
- alumnes / 600
- 1asix / 610
- 2asix / 611
- sudo / 27
- 1wiam / 612
- 2wiam / 613
- 1hiaw / 614
```
dn: cn=professors,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: professors
gidNumber: 601
description: Grup de professors
memberUid: pau
memberUid: pere
memberUid: jordi

dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: alumnes
gidNumber: 600
description: Grup de alumnes
memberUid: anna
memberUid: marta

dn: cn=1asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1asix
gidNumber: 610
description: Grup del primer curs de ASIX
memberUid: user01
memberUid: user02
memberUid: user03
memberUid: user04
memberUid: user05

dn: cn=2asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2asix
gidNumber: 611
description: Grup del segon curs de ASIX
memberUid: user06
memberUid: user07
memberUid: user08
memberUid: user09
memberUid: user10

dn: cn=sudo,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: wheel
gidNumber: 27
description: Grup de sudoers

dn: cn=1wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1wiam
gidNumber: 612
description: Grup del primer curs de WIAM

dn: cn=2wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2wiam
gidNumber: 613
description: Grup del segon curs de WIAM

dn: cn=1hiaw,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1hiaw
gidNumber: 614
description: Grup del primers curs de HIAW
```
#### Fitxer slapd.conf
Comentem els schemas que no siguin necessaris del fitxer **slapd.conf**. Només utilitzarem:
- core.schema
- cosine.schema
- inetorgperson.schema
- nis.schema
- openldap.schema
- collective.schema
```
#include                /etc/ldap/schema/corba.schema
include         /etc/ldap/schema/core.schema
include         /etc/ldap/schema/cosine.schema
#include                /etc/ldap/schema/duaconf.schema
#include                /etc/ldap/schema/dyngroup.schema
include         /etc/ldap/schema/inetorgperson.schema
#include                /etc/ldap/schema/java.schema
#include                /etc/ldap/schema/misc.schema
include         /etc/ldap/schema/nis.schema
include         /etc/ldap/schema/openldap.schema
#include                /etc/ldap/schema/ppolicy.schema
include         /etc/ldap/schema/collective.schema
```
#### Fitxer startup.sh
Modifiquen el fitxer startup.sh de manera que escolti els protocols **ldap, ldaps i ldapi**.
```
#!/bin/bash

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

/usr/sbin/slapd -d0 -h 'ldap:/// ldaps:/// ldapi:///'
```
#### Comprovació
Llistat del OU grups
```
ldapsearch -x -LLL -D 'cn=Manager,dc=edt,dc=org' -w secret -b 'dc=edt,dc=org' ou=grups

dn: ou=grups,dc=edt,dc=org
ou: grups
description: Container per als grups
objectClass: organizationalunit
```
