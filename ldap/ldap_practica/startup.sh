#!/bin/bash
echo "Configurant el servidor ldap..."

# Ldap servers:
# edtasixm06/ldap22:base Server bàsic ldap, amb base de dades edt.org. Aquesta imatge engega amb CMD un script anomenat startup.sh que fa el següent:
#    Esborrar els directoris de configuració i de dades
rm -rf /etc/ldap/slapd.d/* # Directori configuració dinàmica LDAP
rm -rf /var/lib/ldap/* # Directori dades configuració LDAP
#    Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
#    Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif  
#    Assignar la propietat i grup del directori de dades i de configuració a l'usuari openldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
#    Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0 # Els containers que funcionen en detach han d'estar en foreground, en slapd s'ha dutilitzar el nivell 0 de depuració (debug)
