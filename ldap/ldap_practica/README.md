# PRÀCTICA LDAP SCHEMA

## Creació de la imatge ldap:practica

En aquesta pràctica crearem un nou schema que implementarem a la nostra base de dades. Crearem un nou objecte structural, un altre auxiliar i a cada objecte hi crearem els nostres atributs. En el meu cas, he creat un schema sobre ciutats d'Espanya.

#### Desplegament al directori context
```
docker build -t jordijsmx/ldap:practica .

docker run --rm --name ldap -h ldapserver --net 2hisx -p 389:389 -d jordijsmx/ldap:practica
docker run --rm --name phpldapadmin --net 2hisx -p 80:80 -d jordijsmx/phpldapadmin
```

#### Desplegament amb compose
```
docker compose up -d
```

#### Fitxer ciutats.schema
```
# ciutats.schema     
#
# x-nom
# x-provincia
# x-comunitat
# x-poblacio       integer
# x-info	   pdf
# x-mapa           image
# x-aeroport       boolean
# -------------------------------------
attributetype (1.1.2.1.1 NAME 'x-nom'
	DESC 'nom de la ciutat'
	EQUALITY caseIgnoreMatch       
	SUBSTR caseIgnoreSubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
	SINGLE-VALUE )

attributetype (1.1.2.1.2 NAME 'x-provincia'
	DESC 'nom de la provincia'
	EQUALITY caseIgnoreMatch
	SUBSTR caseIgnoreSubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
	SINGLE-VALUE )

attributetype (1.1.2.1.3 NAME 'x-comunitat'
        DESC 'nom de la provincia'
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
        SINGLE-VALUE )

attributetype (1.1.2.1.4 NAME 'x-poblacio'
	DESC 'nombre'
	EQUALITY integerMatch
	ORDERING integerOrderingMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
	SINGLE-VALUE )

attributetype (1.1.2.1.5 NAME 'x-info'
	DESC 'informació de la ciutat'
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.5)

attributetype (1.1.2.1.6 NAME 'x-mapa'
        DESC 'Imatge del mapa'
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.28 )

attributetype (1.1.2.1.7 NAME 'x-aeroport'
        DESC 'aeroport true/false'
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.7 )

objectClass (1.1.2.2.1 NAME 'x-ciutats'
        DESC 'Ciutats del REINO DE ESPAÑA'
        SUP TOP
	STRUCTURAL
        MUST ( x-nom $ x-provincia $ x-comunitat )
        MAY x-poblacio
	)

objectClass (1.1.2.2.2 NAME 'x-informacio'
	DESC 'Informació sobre les ciutats Espanyoles'
	SUP TOP
	AUXILIARY
	MUST x-mapa
	MAY ( x-info $ x-aeroport )
	)
```
#### Fitxer edt-org.ldif
En el nostre fitxer de les dades de la base de dades "edt.org" afegim la nova Organizational Unit "practica" amb 3 noves entitats amb els objectClass del nostre schema.
```
dn: ou=practica,dc=edt,dc=org
ou: practica
description: practica schema
objectclass: organizationalunit

dn: x-nom=cunit,ou=practica,dc=edt,dc=org
objectclass: x-ciutats
objectclass: x-informacio
x-nom: cunit
x-provincia: tarragona
x-comunitat: catalunya
x-poblacio: 14622
x-info:< file:/opt/docker/cunit.pdf
x-mapa:< file:/opt/docker/cunit.png
x-aeroport: FALSE

dn: x-nom=lleida,ou=practica,dc=edt,dc=org
objectclass: x-ciutats
objectclass: x-informacio
x-nom: lleida
x-provincia: lleida
x-comunitat: catalunya
x-poblacio: 140797
x-info:< file:/opt/docker/lleida.pdf
x-mapa:< file:/opt/docker/lleida.png
x-aeroport: TRUE

dn: x-nom=ribadeo,ou=practica,dc=edt,dc=org
objectclass: x-ciutats
objectclass: x-informacio
x-nom: ribadeo
x-provincia: lugo
x-comunitat: galicia
x-poblacio: 9811
x-info:< file:/opt/docker/ribadeo.pdf
x-mapa:< file:/opt/docker/ribadeo.png
x-aeroport: FALSE
```
#### Fitxer slapd.conf
Esborrarem tots els schema que no siguin necessaris del nostre fitxer de configuració de LDAP.
Hem esborrat:
- corba.schema
- duaconf.schema
- dyngroup.schema
- java.schema
- misc.schema
- openldap.schema
- ppolicy.schema
- collective.schema
```
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/nis.schema
include		/opt/docker/ciutats.schema
```
#### Fitxer compose.yml
```
version: "3"
services:
  ldap:
    image: jordijsmx/ldap:practica
    container_name: ldapserver
    hostname: ldapserver
    ports:
      - "389:389"
    networks:
      - mynet
  phpldapadmin:
    image: jordijsmx/phpldapadmin
    container_name: phpldap
    hostname: phpldap
    ports:
      - "80:80"
    networks:
      - mynet
networks:
  mynet:
```
#### Comprovació
Introduim al nostre navegador **localhost/phpldapadmin** per accedir al gestor gràfic de LDAP. També podem accedir desde dins del contenidor:
```
docker exec -it ldapserver /bin/bash

ldapsearch -x -LLL -D 'cn=Manager,dc=edt,dc=org' -w secret -b 'ou=practica,dc=edt,dc=org' dn
```
