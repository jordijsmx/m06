# Servidor LDAP en detach

## Fitxers necessaris al directori context
```
Dockerfile
edt-org.ldif
slapd.conf
startup.sh
```
## Generar la imatge
```
docker build -t jordijsmx/ldap:base .
```
## Arrancar la imatge
```
docker run --rm -h ldap.edt.org -it jordijsmx/ldap:base /bin/bash
```
## Arrancar la imatge en detach
```
docker run --rm --name ldap -h ldap.edt.org -d jordijsmx/ldap:base
```
## Entrar al container engegat
```
docker exec -it ldap /bin/bash
```
## Consultes desde el container
```
slapcat
slapcat | grep dn
slapcat | grep cn
slapcat | grep mail
```
## Consultes desde fora del container
```
ldapsearch -x -LLL -H ldap://172.17.0.2 -b ‘dc=edt,dc=org’
ldapsearch -x -LLL -H ldap://172.17.0.2 -b ‘dc=edt,dc=org’ dn # Mostra només dn
ldapsearch -x -LLL -H ldap://172.17.0.2 -b ‘dc=edt,dc=org’ cn mail # Mostra common names i mails
ldapsearch -x -LLL -H ldap://172.17.0.2 -b ‘ou=usuaris,dc=edt,dc=org’ # Mostra tot a partir d’usuaris
```
## Esborrar entrades
```
docker exec -it repte4 ldapdelete -vx -H ldap://172.17.0.2 -D 'cn=Manager,dc=edt,dc=org' -w secret 'cn=Pau Pou,ou=usuaris,dc=edt,dc=org'
```
## Creació volums
```
docker volume create ldap-config
docker volume create ldap-data
```
## Arrancar imatge amb persistència de dades
```
docker run --rm --name repte4 -h ldap.edt.org -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap -d jordijsmx/ldap:base
