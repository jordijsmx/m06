# LDAP server

## Exercici LDAP22:editat

### Desplegament
```
docker build -t jordijsmx/ldap:editat .

docker run --rm --name editat --net 2hisx -p 389:389 -d jordijsmx/ldap:editat
```
#### Usuaris amb format UID
```
dn: uid=user01,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user01
cn: usuari 1
sn: usuari 1
homephone: 555-222-0001
mail: user01@edt.org
description: Watch out for this user
ou: usuari
uid: user01
uidNumber: 6001
gidNumber: 70
homeDirectory: /tmp/home/user01
userPassword: {SHA}BJf+TWdP43GUpvywiRPllu9qMH8=
```
#### Dos usuaris nous i OU nova
```
dn: ou=practiques,dc=edt,dc=org
ou: practiques
description: tipus de practiques
objectclass: organizationalunit

dn: cn=fct,ou=practiques,dc=edt,dc=org
objectclass: inetOrgPerson
cn: fct
sn: fct
description: modalitat fct

dn: cn=dual,ou=practiques,dc=edt,dc=org
objectclass: inetOrgPerson
cn: dual
sn: dual
description: modalitat dual
```
#### Password de Manager xifrat
```
slappasswd -s secret
```
```
# rootpw secret
rootpw {SHA}5en6G6MezRroT3XKqkdPOmY/BfQ=
```
#### Base de dades cn=config amb usuari "Sysadmin" i password "syskey".
```
database config
rootdn "cn=Sysadmin,cn=config"
rootpw syskey
```
#### Modificació en calent dels permisos de "edt.org" a partir d'un fitxer:
```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: saturno
-
delete: olcAccess
-
add: olcAccess
olcAccess: to * by * write
```
#### Ordre per modificar i comprovació
```
ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f mod1.ldif

ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcRootPW olcAccess

dn: olcDatabase={1}mdb,cn=config
olcRootPW: saturno
olcAccess: {0}to * by * write
```
#### Canvi de nom d'un usuari a partir d'un fitxer
```
dn: uid=user03,ou=usuaris,dc=edt,dc=org
changetype: modify
replace: cn
cn: Menganito
```
#### Ordre per modificar i comprovació
```
ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f mod2.ldif

ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'uid=user03,ou=usuaris,dc=edt,dc=org'
dn: uid=user03,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
sn: usuari 3
homePhone: 555-222-0003
mail: user03@edt.org
description: Watch out for this user
ou: usuari
uid: user03
uidNumber: 6003
gidNumber: 70
homeDirectory: /tmp/home/user03
userPassword:: e1NIQX1id2tsaUtRMlplSktlUmVTUzZJVzlRKzNjMzA9
cn: Menganito
```
