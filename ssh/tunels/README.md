# Túnel SSH

## Túnel directe

### Desplegar phpldapadmin amb port 80 propagat
Hem d'afegir **ldap.edt.org** a localhost de **/etc/hosts** al container phpldapadmin
```
docker run --rm --name phpldapadmin -h phpldapadmin --net 2hisx -p 80:80 -d jordijsmx/phpldapadmin:latest
```
### Desplegar a AWS container amb servidor ldap.edt.org sense cap propagació.
Al security groups de AWS només ha de permetre l'accés al port 22.

```
ssh -i keyCLI.pem ec2-user@54.82.42.214

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d jordijsmx/ldap:latest
```
Hem d'afegir la IP del container phpldapadmin al **/etc/hosts** de AWS.

### Establir túnel SSH entre host i instància AWS
```
ssh -i keyCLI.pem -L 389:ldap.edt.org:389 ec2-user@IP-AWS
```

## Túnel invers

### 
