# PAM

#### Contingut Dockerfile
```
FROM debian:latest
LABEL version="1.0"
LABEL subject="PAM host"
RUN apt-get update
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount 
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```
#### Contingut startup.sh
S'afegeix **/bin/bash** al final per a que sigui sessió interactiva
```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

/bin/bash
```
#### Desplegament
Amb la opció **--privileged** li indiquem que utilitzi permisos especials. Amb la opció **--capabilities** es poden personalitzar opcions.
```
docker build -t jordijsmx/pam:base .

docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -it jordijsmx/pam:base
```
#### Comprovació
```
tail -5 /etc/shadow
```
