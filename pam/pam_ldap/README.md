# PAM: Autenticació LDAP

### Desplegament
Amb la opció **--privileged** li indiquem que utilitzi permisos especials. Amb la opció **--capabilities** es poden personalitzar opcions.
```
docker build -t jordijsmx/pam:ldap .

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -p 636:636 -d jordijsmx/ldap:latest

docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -it jordijsmx/pam:ldap
```
També es pot desplegar amb **compose**: ```docker compose up -d```
```
version: "3"
services:
  ldap:
    image: jordijsmx/ldap:latest
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
      - "636:636"
    networks:
      - 2hisx
  pam:
    image: jordijsmx/pam:ldap
    container_name: pam.edt.org
    hostname: pam.edt.org
    privileged: true
    networks:
      - 2hisx
networks:
  2hisx:
```


### Contingut Dockerfile
```
FROM debian:latest
LABEL version="1.0"
LABEL subject="PAM host"
RUN apt-get update
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount nslcd nslcd-utils ldap-utils
RUN mkdir /opt/docker
COPY * /opt/docker/
COPY ldap.conf /etc/ldap/
COPY nslcd.conf /etc/
COPY nsswitch.conf /etc/
COPY common-session /etc/pam.d/
COPY pam_mount.conf.xml /etc/security/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```
### Contingut startup.sh
S'afegeix **/bin/bash** al final per a que sigui sessió interactiva
```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

/usr/sbin/nslcd
/usr/sbin/nscd

sleep infinity
/bin/bash
```

### Fitxer /etc/ldap/ldap.conf
Per comprovar la connectivitat el servidor PAM amb el servidor LDAP. D'aquesta manera podrem realitzar consultes amb ldapsearch sense tenir que introduir la IP o el domini de la base de dades.
```
BASE	dc=edt,dc=org
URI	ldap://ldap.edt.org
``` 

### Fitxer /etc/nslcd.conf 
Afegim la informació del nostre servidor LDAP, d'aquesta manera, el servidor PAM serà capaç de poder trobar informació dels comptes usuaris configurats al servidor LDAP.
```
uri ldap://ldap.edt.org 

base dc=edt,dc=org
```

### Fitxer /etc/nsswitch.conf 
Aquest fitxer determina l'origen d'on obtenir l'informació dels usuaris i amb quina prioritat.
```
passwd:         files ldap
group:          files ldap
```

### Engeguem els dimonis
Un cop engegats podrem utilitzar l'ordre **getent passwd [user]** o **getent group [grup]** per buscar informació de comptes d'usuari o grups.
```
/usr/sbin/nscd
/isr/sbin/nslcd
```

### Fitxer /etc/pam.d/common-session
Aquest fitxer determina quins mòduls s'executaran després de que un usuari s'hagi autenticat correctament. Editarem aquest fitxer per a que al iniciar sessió amb un usuari ldap, crei el seu directori home automàticament. Afegirem el mòdul **pam_mkhomedir.so** a la línia indicada.
```
session	[default=1]	pam_permit.so
session	requisite	pam_deny.so
session	required	pam_permit.so

session	required	pam_unix.so 
session optional	pam_mkhomedir.so
session	optional	pam_mount.so 

session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
```

### Fitxer /etc/sexurty/pam_mount.conf.xml
En aquest fitxer configurarem el recurs temporal que tindran els usuaris LDAP quan iniciin sessió. Es creara un directori ramdisk de tipus **tmpfs** de **100M**
```
<!-- Volume definitions -->

<volume
	user="*"
	fstype="tmpfs"
	mountpoint="~/tmp"
	options="size=100M"
/>
```
